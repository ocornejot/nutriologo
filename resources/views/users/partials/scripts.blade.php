@push('js')
 <script>
    console.log("Waskalle Bahena");

    var dTable = $("#users-table").DataTable({
        processing: true,
        serverSide: true,
        ajax: '/users',
        columns: [
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'email_verified_at', name: 'email_verified_at'},
            {data: 'actions', name: 'actions', orderable: false, serchable: false,  bSearchable: false},
        ],
    });//DATATABLE #brands-table
 </script>
@endpush
